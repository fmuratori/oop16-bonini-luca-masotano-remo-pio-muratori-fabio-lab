Progetto d'esame del corso Programmazione ad Oggetti
Anno accademico 2016/2017

Il team si impegna a realizzare un videogioco bidimensionale con visuale dall’alto.
Il gioco sarà ispirato da alcune meccaniche tipiche del genere “roguelike” e conterrà un personaggio che affronterà dei nemici attraverso diverse stanze.

Il gioco sarà dotato delle seguenti funzionalità:
una mappa contenente stanze esplorabili dal giocatore;
varie tipologie di nemici che spareranno contro il giocatore;
i nemici e il giocatore presenteranno diverse statistiche;
una minimappa utile al giocatore per orientarsi in gioco;
un boss che una volta ucciso pone fine al gioco.

Features opzionali:
implementazione di diversi piani, ognuno con un insieme di stanze;
oggetti in grado di modificare le statistiche del giocatore;
ostacoli, presenti in una stanza, che limitano la libertà di giocatore e nemici.

Challenges:
generazione casuale della mappa, così come i nemici dentro ogni stanza;
salvataggio e caricamento della partita;
nemici dotati di una buona “intelligenza” di gioco.

Suddivisione del lavoro:
Bonini Luca: Controller
Masotano Remo Pio:  Model
Muratori Fabio: View

In corso d’opera la suddivisione dei ruoli potrebbe riequilibrarsi in base alle esigenze ed anche l’utilizzo o meno di librerie esterne da identificare.

Deadline scelta: 25/09/2017 (E)